**Show Person**
----
  Returns json data about a single person.

* **URL**

  /people/:id

* **Method:**

  `GET`  
  
*  **URL Params**

   **Required:**
 
   `id=[integer]`

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"id":3,"version":0,"firstname":"jMetter","surname":"test","age":5}`
 

**Show All People**
----
  Returns json data about all people.

* **URL**

  /people

* **Method:**

  `GET`  
  
*  **URL Params**

   none

* **Data Params**

   none

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `[
                    {
                      "id": 3,
                      "version": 0,
                      "firstname": "jMetter",
                      "surname": "test",
                      "age": 5
                      },
                      {
                      "id": 4,
                      "version": 0,
                      "firstname": "jMetter",
                      "surname": "Tests",
                      "age": 5
                      }
                  ]`
 
**Add Person**
----
  Add a single person to database.

* **URL**

  /people

* **Method:**

  `post`  
  
*  **URL Params**
   
   none

* **Data Params**
    
   `aplication/json` <br /> 
   firstname <br />
   surname <br />
   age 
   
* **Success Response:**

  * **Code:** 201 <br />
    
 

**Delete Person**
----
  Delete a single person.

* **URL**

  /people/:id

* **Method:**

  `DELETE`  
  
*  **URL Params**

   **Required:**
 
   `id=[integer]`

* **Data Params**

  None

* **Success Response:**

  * **Code:** 204 <br />
    
 
 **Update Person**
 ----
   Update person.
 
 * **URL**
 
   /people/:id
 
 * **Method:**
 
   `PUT`  
   
 *  **URL Params**
 
    **Required:**
  
    `id=[integer]`
 
 * **Data Params**
 
   id <br />
   firstname <br />
   surname <br />
   age 
 
 * **Success Response:**
 
   * **Code:** 204 <br />
     
  